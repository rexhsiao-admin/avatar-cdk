const sharp = require("sharp");
const AWS = require("aws-sdk");
const S3 = new AWS.S3();

exports.handler = async function (event, context) {
  const { Records } = event;
  let bucket;
  let file;
  let width = 300;
  let height = 300;

  try {
    Records.map(async (record) => {
      bucket = record.s3.bucket.name;
      file = record.s3.object.key;
    });

    await Promise.all(resizeImage(bucket, file, width, height));

    return {
      statusCode: 200,
      headers: {},
      body: JSON.stringify({ message: "Successfully uploaded file to S3" }),
    };
  } catch (error) {
    var body = error.stack || JSON.stringify(error, null, 2);
    return {
      statusCode: 400,
      headers: {},
      body: body,
    };
  }
};

const resizeImage = async (bucket, file, width, height) => {
  const imageBuffer = await S3.get(file, bucket);
  const sharpImageBuffer = await sharp(imageBuffer)
    .resize(width, height)
    .toBuffer();

  const shortFileName = file.split("/")[1];
  const newFileName = `resized/${width}x${height}/${shortFileName}.webp`;

  await S3.putObject({
    Bucket: bucket,
    Key: newFileName,
    Body: sharpImageBuffer,
    ContentEncoding: "base64",
    ContentType: "image/webp",
  }).promise();

  console.log(newFileName);

  return newFileName;
};
