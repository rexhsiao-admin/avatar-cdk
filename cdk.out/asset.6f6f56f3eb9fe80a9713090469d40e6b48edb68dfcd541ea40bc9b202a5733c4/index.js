const sharp = require("sharp");
const AWS = require("aws-sdk");
const S3 = new AWS.S3();

exports.handler = async function (event, context) {
  const { Records } = event;

  try {
    const promArray = Records.map(async (record) => {
      const bucket = record.s3.bucket.name;
      const file = record.s3.object.key;
      console.log("bucket:", bucket);
      console.log("key:", file);
      const width = 300;
      const height = 300;
      const image = await resizeImage(bucket, file, width, height);
      console.log(image);
    });

    await Promise.all(promArray);

    return {
      statusCode: 200,
      headers: {},
      body: JSON.stringify({ message: "Successfully uploaded file to S3" }),
    };
  } catch (error) {
    var body = error.stack || JSON.stringify(error, null, 2);
    return {
      statusCode: 400,
      headers: {},
      body: body,
    };
  }
};

const resizeImage = async (bucket, file, width, height) => {
  const imageBuffer = await S3.get(file, bucket);
  const sharpImageBuffer = await sharp(imageBuffer)
    .resize(width, height)
    .toBuffer();

  const shortFileName = file.split("/")[1];
  const newFileName = `resized/${width}x${height}/${shortFileName}.webp`;

  await S3.putObject({
    Bucket: bucket,
    Key: newFileName,
    Body: sharpImageBuffer,
    ContentEncoding: "base64",
    ContentType: "image/webp",
  }).promise();

  console.log(newFileName);

  return newFileName;
};
