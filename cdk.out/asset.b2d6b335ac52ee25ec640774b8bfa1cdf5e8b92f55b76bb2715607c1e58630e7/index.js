const AWS = require("aws-sdk");
const S3 = new AWS.S3();

const bucketName = process.env.BUCKET;
const region = process.env.REGION;

exports.handler = async function (event, context) {
  try {
    var method = event.httpMethod;
    var widgetName = event.path.startsWith("/")
      ? event.path.substring(1)
      : event.path;

    if (method === "GET") {
      // GET / to get the names of all widgets
      if (event.path === "/") {
        const data = await S3.listObjectsV2({ Bucket: bucketName }).promise();
        var body = {
          widgets: data.Contents.map(function (e) {
            return e.Key;
          }),
        };
        return {
          statusCode: 200,
          headers: {},
          body: JSON.stringify(body),
        };
      }

      if (widgetName) {
        // GET /name to get info on widget name
        const data = await S3.getObject({
          Bucket: bucketName,
          Key: widgetName,
        }).promise();
        var body = data.Body.toString("utf-8");

        return {
          statusCode: 200,
          headers: {},
          body: JSON.stringify(body),
        };
      }
    }

    if (method === "POST") {
      if (event.path === "/") {
        const now = new Date();
        var data = widgetName + " created: " + now;
        const { image } = JSON.parse(event.body);

        const key =
          Math.random().toString(36).substring(2, 10) +
          Math.random().toString(36).substring(2, 10);

        const parts = image.split(";");
        const mime = parts[0].split(":")[1];

        var body = new Buffer.from(
          image.replace(/^data:image\/\w+;base64,/, ""),
          "base64"
        );

        await S3.putObject({
          Bucket: bucketName,
          Key: `photos/${key}`,
          Body: body,
          ContentEncoding: "base64",
          ContentType: mime,
        }).promise();
        https://cdkstack-homeplusavatar2370a12d-roxoybyum2al.s3.us-east-1.amazonaws.com/photos/f1lbnkqv6f9qg37y?response-content-disposition=inline&X-Amz-Security-Token=IQoJb3JpZ2luX2VjECkaDmFwLXNvdXRoZWFzdC0yIkcwRQIgG%2B3MjRsI7JJHUojqcgcoU9ceHwpIZqtP%2BiXP7d5XzP4CIQCRd5%2By58fLGNKRd6QLM9N1W6335KZzPWxBeYnpb%2FgruSrtAgiy%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F8BEAAaDDgxNDEwNjgwNzg3OCIM%2BAzT%2F040ZaIxwDhuKsECTgKSb%2BAPyB1heRTGhVVsI6HG8WVIM3K15hu4sW1VjXLpxupRd2Lw9j2EUIzQbn%2FJZr96voWv6dNpGMAUQoDfLqNglXHsqnsr5ADb1Yun9d5dQA1lIIAqxOtsMFmnMv8pSlRCScpdjdmXo6icA1VG7T0lZQ%2Fvc3iPMagRIf1t6m00LTB0GTlren%2BkyvDxT8S3isQPf9zTp%2BWd8hqSWyrUF8sOrAB6cDOibwJg1xhlKeecfkZAhcodSQMYuyIGmOIfw1vid5c4T00%2FFSu%2BMtFMw7bdh8EHP3vsBdxSBzqQ3lMWgdm8%2BFPFsiY8kcTzJ%2FUGKxFzzTrCF%2B7LaiVnhJ%2BXQezLrLnGSkTdI%2BQHoUfmii8RjEcHwbeY1zmXR%2FfpCu2nBrCC591Reav6YS8ov9I%2FggzXKOQdNx5Qxg43pa1qWcp5MP%2BNhJIGOrMCWf2UDr7YWztViTcdTnYlgR1iZwHtgUg0%2F1QYQhNBGndwhj99WFKWpYbN1hD4kywA0urkeDtQm%2BkIoyNGHOudiqU%2FL8JBerbM4%2FgCpE21YKve5cWZX5mOmlqkIM3LemgqRV2cgy8qiIHoTa21Hil3Et1b4%2FZsWViaY0oVwA3TLyyiXZisiUncvoCRjVT29TrtOAqiQ%2FFhg90Pnwi33fBii27AAwDBz7tphiwv5Y244IynSQvGdK9azASlLPoeBKcZU%2Bnqx1ALKW6jDFjrbtgKljZ2XcuHZiNdnpeIuReQ1nFx9Ilev3ULft3uvH9msHePtgSiGNFsdiLNMAx8APm2KaaErPbKkI0%2Botdbgm%2BMTeTkquzFkrUR0BnLPkY%2BUSP26te2KqQUGeUJpTi6Zquf1K6r6Q%3D%3D&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20220328T124023Z&X-Amz-SignedHeaders=host&X-Amz-Expires=299&X-Amz-Credential=ASIA33DEMBZDDQ7I62W2%2F20220328%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Signature=3cc9ac6d13c039222ce0546f18b3873a886bfe8811898dbceb547eb8b40cc5a3

        const url = `https://cdkstack-homeplusavatar2370a12d-roxoybyum2al.s3-${region}.amazonaws.com/photos/${key}`;

        return {
          statusCode: 200,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Headers": "*",
            "Access-Control-Allow-Methods": "OPTIONS,POST,GET",
          },
          body: JSON.stringify(url),
        };
      }
    }

    return {
      statusCode: 400,
      headers: {},
      body: "We only accept GET and POST /",
    };
  } catch (error) {
    var body = error.stack || JSON.stringify(error, null, 2);
    return {
      statusCode: 400,
      headers: {},
      body: body,
    };
  }
};
