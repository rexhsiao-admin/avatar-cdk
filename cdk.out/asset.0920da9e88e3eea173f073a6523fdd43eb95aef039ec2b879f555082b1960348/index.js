const { v4: uuidv4 } = require("uuid");
const AWS = require("aws-sdk");
const S3 = new AWS.S3();

const bucketName = process.env.BUCKET;
const region = process.env.REGION;

exports.handler = async function (event, context) {
  try {
    var method = event.httpMethod;
    const { imageData, mime } = JSON.parse(event.body);

    if (method === "POST") {
      if (event.path === "/upload") {
        const body = Buffer.from(imageData, "base64");
        const key = uuidv4();
        const data = await S3.putObject({
          Body: body,
          Key: key,
          ContentType: mime,
          Bucket: bucketName,
          ACL: "public-read",
        }).promise();

        const url = `https://${bucketName}.s3-${region}.amazonaws.com/${key}.${mime}`;

        var body = {
          url,
        };

        return {
          statusCode: 200,
          headers: {},
          body: JSON.stringify(body),
        };
      }
    }

    // We only accept GET for now
    return {
      statusCode: 400,
      headers: {},
      body: "We only accept POST /",
    };
  } catch (error) {
    var body = error.stack || JSON.stringify(error, null, 2);
    return {
      statusCode: 400,
      headers: {},
      body: JSON.stringify(body),
    };
  }
};
